# Test Ifca

Test Developer IFCA
Naufal Akmal Fauzi

# Start Project
```
git clone git@gitlab.com:simple-project4/test-ifca.git
cd test-ifca
npm install
npm start
```

# Screen Shot
Home Page <br>
<img src="./images/home.png">

Add New Page <br>
<img src="./images/new.png">

View Page <br>
<img src="./images/view.png">

# Script Test Yang Lain
```
Test Logika dan Performance
localhost:3000/test
```
<img src="./images/test.png">

```
Test SQL
./test_sql.sql
```