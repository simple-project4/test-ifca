import { createSlice } from '@reduxjs/toolkit';

export const ParamPage = createSlice({
    name: 'paramPage',
    'initialState': {
        value: {}
    },
    reducers: {
        setParamPage: (state, action) => {
            state.value = action.payload;
        },
        resetParamPage: (state) => {
            state.value = {};
        },
    }
});

export const { setParamPage, resetParamPage } = ParamPage.actions;

export default ParamPage.reducer;