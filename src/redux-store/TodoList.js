import { createSlice } from '@reduxjs/toolkit';

export const TodoList = createSlice({
    name: 'todolist',
    'initialState': {
        value: []
    },
    reducers: {
        set: (state, action) => {
            state.value = action.payload;
        },
    }
});

export const { set } = TodoList.actions;

export default TodoList.reducer;