import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { set as SetTodoList } from '../redux-store/TodoList'
import { setLoader } from '../redux-store/Loader'
import { setParamPage } from '../redux-store/ParamPage'
import { useNavigate } from 'react-router-dom'

import icon_plus from '../icons/icon-plus.svg';
import './views.css';

function Home() {
    const route = useNavigate();
    const [Todo, setTodo] = useState([]);
    const [Search, setSearch] = useState('');
    const Rex_Todo = useSelector((state) => state.todolist.value);
    const dp_load = useDispatch();
    const dp_rex = useDispatch();
    const dp_param = useDispatch();

    const DoEdit = (data, index) => {
        // const paramPage = useSelector((state) => state.paramPage.value);
        const param = {
            ...data,
            index: index,
            isEdit: true
        };

        dp_param(setParamPage(param));
        route('/form');
    }

    const onSearch = (e) => {
        let data = e.target.value;
        const List = Rex_Todo;
        setSearch(data);
        if (data && data !== '') {
            let filter_array = List.filter((el) => el.name.toLowerCase().includes(data.toLowerCase()) );
            setTodo(filter_array);
        } else {
            setTodo(Rex_Todo);
        }
    }

    useEffect(() => {
        const mounted = async () => {
            if (Array.isArray(Rex_Todo) && Rex_Todo.length > 0) {
                setTodo(Rex_Todo);
            } else {
                dp_load(setLoader(true));
                await fetch(
                    "https://www.boredapi.com/api/activity?type=education"
                )
                .then(res => res.json())
                .then((result) => {
                    let ress = [{
                        name: "Learn Express.js",
                        description: "https://expressjs.com/",
                        status: 'Y',
                    }];
                    ress.push({
                        name: result.activity,
                        description: result.link,
                        status: 'N'
                    });
                    dp_rex(SetTodoList(ress));
                    setTodo(ress);
                    dp_load(setLoader(false));
                },
                (error) => {
                    dp_load(setLoader(false));
                });
            }
        }

        mounted();

        // return () => {

        // }
    }, [])

    return (
        <>
            <div className='fixed'>
                <div className='header-wrap'>
                    <div className='title'>
                        <div className='title-text'>To Do</div>
                    </div>
                    <div className='icon'>
                        <img src={icon_plus} alt="none" onClick={() => route('/form')} />
                    </div>
                </div>
                <div className='search-bar'>
                    <div className='form-wrapper'>
                        <div>
                            <input type='text' placeholder='Search ...' value={Search} className='form-control' onChange={onSearch} />
                        </div>
                    </div>
                </div>
                <div className='content-wrap'>
                    {Todo.map((data, idx) =>(
                        <div key={idx} className={`todo-list-wrap ${data.status == 'Y' && 'done'}`} onClick={() => DoEdit(data, idx)}>
                            <div className={`todo-list-text ${data.status == 'Y' && 'done'}`}>
                                {data.name}
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </>
    );
}

export default Home;
