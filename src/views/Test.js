import React from 'react'

const PerfectTwo = (n = 0) => {
    if (n < 1) return false;
    const x = [2,7,11,15];
    let id1 = null, id2 = null;

    x.some((datax, idx, arrx) => {
        let stts = false;
        arrx.some((el, id) => {
            let sum = (datax + el);
            if (sum === n) {
                id1 = idx;
                id2 = id;
                stts = true;
            }
            return stts;
        })

        return stts;
    })

    console.log(id1, id2);
}

const LogikaIT = (n = '') => {
    if (n == '') return false;
    const x = ['pro', 'gram', 'merit', 'program', 'it', 'programmer'];
    const ress = [];

    x.some((datax) => {
        if (n.includes(datax)) {
            ress.push(datax);
        }
    })

    console.log(ress);
}

const Palindrome = (n = 0) => {
    let numb = parseInt("9".repeat(n));
    let max_loop = parseInt("1" + "0".repeat(n-1));
    let temp_numb = []
    for (let i = numb; i > max_loop; i--) {
        for (let j = numb; j > max_loop; j--) {
            let x = j*i;
            let y = parseInt(x.toString().split('').reverse().join(''));
            if (x == y) {
                temp_numb.push(x);
            }
        }
    }

    let max = Math.max.apply(Math, temp_numb);
    console.log(max);
}

const Saham = (n = []) => {
    let min = Math.min.apply(Math, n);
    let idx_min = n.indexOf(min);

    let arr_ress = n.splice(idx_min, (n.length - idx_min));
    let max = Math.max.apply(Math, arr_ress);
    
    console.log((max - min) <= 0 ? 'Harga Selalu Turun' : max - min);
}

const MagicNumb = (n = 0, kons = 0) => {
    let status = (n == 6174);
    console.log(n, kons)
    if (status) return n;
    kons ++;
    let min = n.toString().split('').sort((a, b) => a - b).join('');
    let max = n.toString().split('').sort((a, b) => b - a).join('');
    let num = parseInt(max) - parseInt(min);
    MagicNumb(num, kons);
}

const Origami = (p = 20, l = 20) => {
    let squares = p*l;
    let steps = false;
    console.log(`The Initial Dimension is ${p}cm x ${l}cm`);
    while (squares > 1) {
        if (steps) {
            p = p/2;
        } else {
            l = l/2;
        }

        squares = p*l;
        squares > 1 ? console.log(`The Dimension is ${p}cm x ${l}cm`) : console.log("The Dimension Finally Less Than 1cm");
        steps = !steps;
    }
}

const Prime = (n = 0) => {
    if (n < 2 || n > 1000000) {
        console.log("Number Not Valid");
        return "Number Not Valid";
    }
    for (let i = 2; i <= n; i++) {
        if (i%2 > 0) {
            console.log(`${i} is Prime Number`);
        }
    }
}


function Test() {
    // Logika Test
    PerfectTwo(9);
    LogikaIT('programmerit');
    Palindrome(2);
    Saham([50, 40, 20, 11, 6, 1]);
    MagicNumb(3124, 0);

    // Performance Test
    Origami(20,20);
    Prime(13);
    return (
        <>
            <div className='text-center w-full h-full'>
                <h1>TEST IFCA</h1>
            </div>
        </>
    )
}

export default Test
