import { useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect, useState } from 'react';
import { set as SetTodoList } from '../redux-store/TodoList'
import { setLoader } from '../redux-store/Loader'
import { resetParamPage } from '../redux-store/ParamPage'

import icon_back from '../icons/icon-back.png';
import icon_done from '../icons/icon-done.svg';

function AddTodo() {
    const [judul, setJudul] = useState('Add New');
    const [status_form, setStatusForm] = useState(true);
    const route = useNavigate();
    const paramPage = useSelector((state) => state.paramPage.value);
    const Todo = useSelector((state) => state.todolist.value);
    const dp_Todo = useDispatch();
    const dp_load = useDispatch();
    const dp_param = useDispatch();

    const [Model, setModel] = useState({
        name: '',
        description: '',
        status: false
    });

    const doBack = () => {
        route(-1);
    }

    const submitForm = () => {
        dp_load(setLoader(true));
        if (paramPage.isEdit) {
            editForm();
        } else {
            saveForm();
        }
    };

    const saveForm = () => {
        if (Array.isArray(Todo)) {
            let data = [];
            if (Todo.length > 0) {
                data = JSON.parse(JSON.stringify(Todo));
            }
            console.log(data)
            data.push({
                name: Model.name,
                description: Model.description,
                status: 'N',
            });
            dp_Todo(SetTodoList(data));

            setTimeout(() => {
                dp_load(setLoader(false));
                route(-1);
            }, 100);
        } else {
            dp_load(setLoader(false));
            alert('error')
        }
    };
    const editForm = () => {
        if (paramPage.index) {
            let data = JSON.parse(JSON.stringify(Todo));
            data[paramPage.index].name = Model.name;
            data[paramPage.index].description = Model.description;
            data[paramPage.index].status = Model.status ? 'Y' : 'N';
            dp_Todo(SetTodoList(data));

            setTimeout(() => {
                dp_load(setLoader(false));
                route(-1);
            }, 100);
        } else {
            dp_load(setLoader(false));
            alert('error')
        }
    };

    useEffect(() => {
        const setData = async () => {
            if (paramPage && Object.keys(paramPage).length > 0) {
                setModel(paramPage);
                setStatusForm(paramPage.status !== 'Y');

                if (paramPage.isEdit) {
                    setJudul("Edit To Do");
                    if (paramPage.status == 'Y') {
                        setJudul("View To Do");
                    }
                }
            }
        }

        const resetData = async () => {
            dp_param(resetParamPage());
            setModel({
                name: '',
                description: '',
                status: false,
            });
            setStatusForm(true);
        }

        setData();
        return () => {
            resetData();
        }
    }, []);

    return (
        <>
            <div className='fixed'>
                <div className='header-wrap'>
                    <div className='icon'>
                        <img src={icon_back} alt="none" onClick={doBack} />
                    </div>
                    <div className='title'>
                        <div className='title-text'>{judul}</div>
                    </div>
                    {(!paramPage && !paramPage.status) || paramPage.status !== 'Y' &&
                        <div className='icon'>
                            <img src={icon_done} alt="none" onClick={submitForm} />
                        </div>
                    }
                </div>
                <div className='content-form'>
                    <form>
                        <div className='form-wrapper'>
                            <label>Name</label>
                            <div>
                                <input type='text' value={Model.name} className='form-control' required disabled={!status_form} onChange={(e) => setModel({...Model, name: e.target.value})} />
                            </div>
                        </div>
                        <div className='form-wrapper'>
                            <label>Description</label>
                            <div>
                                <textarea value={Model.description} className='form-control' required disabled={!status_form} onChange={(e) => setModel({...Model, description: e.target.value})} />
                            </div>
                        </div>
                        {(paramPage.isEdit && paramPage.status == 'N') &&
                            <div className='form-wrapper'>
                                <label>Mark As Done</label>
                                <div>
                                    <label className='switch'>
                                        <input type='checkbox' value={Model.status} onChange={(e) => setModel({...Model, status: e.target.value})} />
                                        <span className='slider round'></span>
                                    </label>
                                </div>
                            </div>
                        }
                        {/* <div class="form-check form-switch">
                            <input class="form-check-input appearance-none w-9 -ml-10 rounded-full float-left h-5 align-top bg-white bg-no-repeat bg-contain bg-gray-300 focus:outline-none cursor-pointer shadow-sm" type="checkbox" role="switch" id="flexSwitchCheckDefault">
                            <label class="form-check-label inline-block text-gray-800" for="flexSwitchCheckDefault">Default switch checkbox input</label>
                        </div> */}
                    </form>
                </div>
            </div>
        </>
    )
}

export default AddTodo
