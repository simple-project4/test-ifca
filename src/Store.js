import { configureStore } from "@reduxjs/toolkit";
import TodoListReducer from './redux-store/TodoList'
import Loader from './redux-store/Loader'
import ParamPage from "./redux-store/ParamPage";

export default configureStore({
    reducer: {
        todolist: TodoListReducer,
        loader: Loader,
        paramPage: ParamPage
    }
})
