import { BrowserRouter as Router, Routes, Route, useLocation } from "react-router-dom";
import { useSelector } from 'react-redux'

import Home from "./views/Home";
import Forms from "./views/AddTodo";
import Test from "./views/Test";
import { CSSTransition, TransitionGroup } from "react-transition-group";

import './App.css';

function App() {
  const location = useLocation();
  const loader = useSelector((state) => state.loader.value);
  return (
    <>
      {
        loader &&
        <div className="loading">
          <div className="loader"></div>
        </div>
      }
      <TransitionGroup component={null}>
        <CSSTransition key={location.key} classNames="slide" timeout={500}>
          <Routes location={location}>
            <Route path='/' element={<Home />} />
            <Route path='/form' element={<Forms />} />
            <Route path='/test' element={<Test />} />
          </Routes>
        </CSSTransition>
      </TransitionGroup>
    </>
  );
}

const rend = () => <Router><App /></Router>

export default rend;
