SQL Query 1.

SELECT Ponsel.ID, Ponsel.Merek, Ponsel.Model, Ponsel.Jaringan,
CASE Ponsel.DualSim WHEN TRUE THEN 'Ya' ELSE 'Tidak' END,
COUNT(Ponsel.Model), Ponsel.RAM
FROM Ponsel
WHERE Ponsel.DualSim = TRUE
GROUP BY Ponsel.Model

SQL Query 2.

SELECT ps.TempatTinggal
FROM PahlawanSuper as ps INNER JOIN Senjata as sj ON ps.ID = sj.PahlawanSuperID
INNER JOIN JenisSenjata as js ON sj.JenisSenjataID = js.ID
WHERE js.JenisSenjata = 'Kapak'
GROUP BY ps.TempatTinggal

SQL Query 3.

SELECT TOP 100
pd.Nama, pd.TempatTinggal
FROM Pasangan as ps INNER JOIN Perpisahan as pp ON ps.PasanganID = pp.PasanganID
INNER JOIN Penduduk as pd ON ps.PendudukID = pd.ID
ORDER BY TanggalPisah DESC
GROUP BY pd.Nama, pd.TempatTinggal